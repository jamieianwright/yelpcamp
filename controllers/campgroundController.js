var Campground = require("../models/campgroundModel");
var Comments = require("../models/commentModel");
var campgroundController = {};

campgroundController.campgroundIndex = function(req, res) {
	Campground.find({}, function (err, allcampgrounds) {
		if(err){
			console.log(err);
		}
		else{
			res.render("campgrounds/index", {campGrounds:allcampgrounds});
			// console.log(req.isAuthenticated());
			// console.log(req.user);
		}
	});

};

campgroundController.campgroundNew = function(req, res) {
	res.render("campgrounds/new");
};

campgroundController.campgroundCreate = function(req, res){
	var name = req.body.name;
	var image = req.body.image;
	var desc = req.body.description;
	var price = req.body.price;
	var author = {
		id: req.user._id,
		username: req.user.username
	};
	var newCampground = {name: name, image: image, price:price, description: desc, author: author};
    Campground.create(newCampground, 
		function(err, campground){
			if(err){
				console.log(err);
				req.flash("error", err.message);
				res.redirect("/campgrounds/new");
			} else{
				// console.log("A new campground has been added:");
				// console.log(campground);
				req.flash("success", "You created campground " + newCampground.name);
				res.redirect("/campgrounds");
			}
		});
};

campgroundController.campgroundShow = function(req, res) {
	Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
		if(err){
			console.log(err);
		}else{
			res.render("campgrounds/show", {campGround:foundCampground});
		}
	});
};

campgroundController.campgroundEdit = function(req, res) {
	Campground.findById(req.params.id, function(err, foundCampground) {
		if(err){
			console.log(err);
		} else {
			res.render("campgrounds/edit", {campGround:foundCampground});
		}
	});
}

campgroundController.campgroundUpdate = function (req, res) {
	Campground.findByIdAndUpdate(req.params.id, req.body.campground, function (err, updateCampground) {
		if(err){
			console.log(err);
			res.redirect("/campgrounds");
		} else{
			req.flash("success", "You updated the " + updateCampground.name + " campground ");
			res.redirect("/campgrounds/" + req.params.id);
		}
	} );
};

campgroundController.campgroundDestroy = function (req, res) {
	Campground.findById(req.params.id, function(err, foundCampground) {
    	if(err){
			console.log(err);
			res.redirect("/campgrounds");
		} else {
			Comments.remove({_id: {
				$in: foundCampground.comments
			}}, function (err) {
				if(err){
					console.log(err);
				}
			});
			foundCampground.remove(function (err, deletedCampground) {
				if(err){
					console.log(err);
					res.redirect("/campgrounds");
				} else{
					req.flash("success", "You deleted your campground: " + deletedCampground.name);
					res.redirect("/campgrounds");
				}
			});
		}	
	});
};

module.exports = campgroundController;