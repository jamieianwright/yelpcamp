var Campground = require("../models/campgroundModel");
var Comment = require("../models/commentModel");
var commentController = {};

commentController.commentNew = function(req, res) {
    Campground.findById(req.params.id, function(err, foundCampground) {
		if(err){
			console.log(err);
		}else{
			res.render("comments/new", {campGround:foundCampground});
		}
	});
};

commentController.commentCreate = function(req, res){
	Campground.findById(req.params.id, function(err, foundCampground) {
		if(err){
			console.log(err)
		}else{
			Comment.create(req.body.comment, function(err, comment){
				if(err){
					console.log(err)
				} else{
					comment.author.username = req.user.username;
					comment.author.id = req.user.id
					comment.save();
					foundCampground.comments.push(comment);
					foundCampground.save();
					req.flash("success", "You created a new comment about " + foundCampground.name);
					res.redirect("/campgrounds/" + req.params.id)
				}
		})
		}
	})
}

commentController.commentEdit = function(req, res) {
	Comment.findById(req.params.comment_id, function(err, foundComment) {
		if(err){
			console.log(err)
		} else {
			res.render("comments/edit", {campground_id:req.params.id, comment:foundComment});
		}
	})
};

commentController.commentUpdate = function (req, res) {
	Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function (err, updateComment) {
		if(err){
			console.log(err)
			res.redirect("/campgrounds");
		} else{
			req.flash("success", "You updated your comment");
			res.redirect("/campgrounds/" + req.params.id);
		}
	} );
};

commentController.commentDestroy = function (req, res) {
	Comment.findByIdAndRemove(req.params.comment_id, function(err, removedComment){
		if(err){
			console.log(err);
			res.redirect("/campgrounds");
		} else {
			Campground.findByIdAndUpdate(req.params.id, { $pull: {comments: req.params.comment_id} }, function(err, campground){
				if(err){
					console.log(err)
				} else{
					req.flash("error", "Your comment about " + campground.name + " has been deleted");
					res.redirect("/campgrounds/" + req.params.id)
				}
			})	
		}
	})
};

module.exports = commentController;