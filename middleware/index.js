var Campground = require("../models/campgroundModel");
var Comment = require("../models/commentModel");
var middlewareObj = {};


middlewareObj.isLoggedIn = function (req, res, next) {
    if(req.isAuthenticated()){
        return next();
    } else {
    	req.session.returnTo = req.headers.referer;
    	console.log(req.session.returnTo)
    	req.flash("error", "Please Login First!");
    	res.redirect("/login");
    }
}

middlewareObj.checkCampgroundOwnership = function (req, res, next) {
	if(req.isAuthenticated()){
		Campground.findById(req.params.id, function(err, foundCampground) {
			if(err){
				console.log(err)
			} else {
				if(foundCampground.author.id.equals(req.user._id)){
					next();
				} else {
					req.flash("error", "This campground was not created by you.");
					res.redirect("back");
				}
			}
		})
	} else {
		req.flash("error", "Please Login First!");
		res.redirect("/login");
	}
}

middlewareObj.checkCommentOwnership = function (req, res, next) {
	if(req.isAuthenticated()){
		Comment.findById(req.params.comment_id, function(err, foundComment) {
			if(err){
				console.log(err)
			} else {
				if(foundComment.author.id.equals(req.user._id)){
					next();
				} else {
					req.flash("error", "This comment was not created by you.");
					res.redirect("back");
				}
			}
		})
	} else {
		req.flash("error", "Please Login First!");
		res.redirect("/login");
	}
}

middlewareObj.sanitizeCampground = function (req, res, next) {
	req.body.name = req.sanitize(req.body.name);
	req.body.description = req.sanitize(req.body.description);
	next();
}

middlewareObj.sanitizeComment = function (req, res, next) {
	console.log(req.body.comment.text);
	req.body.comment.text = req.sanitize(req.body.comment.text);
	console.log(req.body.comment.text);
	next();
}

module.exports = middlewareObj;