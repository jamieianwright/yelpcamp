var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var expressSanitizer = require("express-sanitizer");
var flash = require("connect-flash");
var mongoose = require("mongoose");
var passport = require("passport");
var localStrategy = require("passport-local");
var passportLocalMongoose = require("passport-local-mongoose");
var User = require("./models/userModel");
var campgroundRoutes = require("./routes/campgroundsRouter");
var commentRoutes = require("./routes/commentsRouter");
var indexRoutes = require("./routes/indexRouter");

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());
app.use(express.static(__dirname + "/public"))
app.use(methodOverride("_method"));
app.use(flash());
console.log(process.env.DATABASEURL);
//mongoose.connect("mongodb://localhost:27017/yelpcamp", { useNewUrlParser: true });
mongoose.connect(process.env.DATABASEURL, { useNewUrlParser: true });



//========================================
//PASSPORT CONFIG
//========================================

app.use(require("express-session")({
    secret: "Jamie is a coding master",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
app.use(function(req, res, next){
	res.locals.currentUser = req.user;
	res.locals.error = req.flash("error");
	res.locals.success = req.flash("success");
	next();
});

app.listen(process.env.PORT, process.env.IP, function(){
	console.log("Server has started");
})

app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);
app.use(indexRoutes);
