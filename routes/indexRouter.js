var express = require("express");
var router = express.Router({mergeParams: true});
var User = require("../models/userModel");
var passport = require("passport");

router.get("/", function(req, res) {
	res.render("landing");
});

//===================================================================
//AUTH ROUTES
//===================================================================

//GET ROUTE FOR REGISTER FORM
router.get("/register", function(req, res) {
    res.render("register");
});

//POST ROUTE FOR REGISTER FORM
router.post("/register", function(req, res){
	//Create new user with username and password
    User.register(new User({username: req.body.username}), req.body.password, function(err, user){
        if(err){
          req.flash("error", err.message);
          return res.redirect("/register");
        }
        //Log in new user
        passport.authenticate("local")(req, res, function(){
        	console.log(user);
        	req.flash("success", "Successfully registered as " + user.username);
            res.redirect("/campgrounds");
        });
    });
    
});

//GET ROUTE FOR LOG IN FORM
router.get("/login", function(req, res) {
    res.render("login");
});

//POST ROUTE FOR LOG IN FORM
router.post("/login", passport.authenticate("local", {
    failureRedirect: "/login",
    failureFlash: true,
}),  function(req, res){
    req.flash("success", "Successfully logged in as " + req.user.username);
    res.redirect(req.session.returnTo || '/');
});

//GET ROUTE FOR LOG OUT 
router.get("/logout", function(req, res) {
    req.logout();
    req.flash("success", "Success, you have logged out!");
    res.redirect("back");
});

module.exports = router;
