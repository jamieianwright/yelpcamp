var express = require("express");
var router = express.Router({mergeParams: true});
var Middleware = require("../middleware");
var CommentController = require("../controllers/commentController");

//===================================================================
//COMMENTS ROUTES
//===================================================================

//NEW COMMENT ROUTE
router.get("/new", Middleware.isLoggedIn, CommentController.commentNew);

//CREATE COMMENT ROUTE
router.post("/", Middleware.isLoggedIn, Middleware.sanitizeComment, CommentController.commentCreate)

//EDIT COMMENT ROUTE
router.get("/:comment_id/edit", Middleware.checkCommentOwnership, CommentController.commentEdit)

//UPDATE COMMENT ROUTE
router.put("/:comment_id", Middleware.checkCommentOwnership, Middleware.sanitizeComment, CommentController.commentUpdate);

//DELETE COMMENT ROUTE
router.delete("/:comment_id", Middleware.checkCommentOwnership, CommentController.commentDestroy);

module.exports = router;
