var express = require("express");
var router = express.Router({mergeParams: true});
var CampgroundController = require("../controllers/campgroundController");
var Middleware = require("../middleware");

//===================================================================
//CAMPGROUND ROUTES
//===================================================================

//index route
router.get("/", CampgroundController.campgroundIndex);

//new route
router.get("/new", Middleware.isLoggedIn, CampgroundController.campgroundNew);

//create route
router.post("/", Middleware.isLoggedIn, Middleware.sanitizeCampground, CampgroundController.campgroundCreate);

//show route
router.get("/:id", CampgroundController.campgroundShow);

//Edit route
router.get("/:id/edit", Middleware.checkCampgroundOwnership, CampgroundController.campgroundEdit);

//Update route
//Requires npm install method-override --save
router.put("/:id", Middleware.checkCampgroundOwnership, Middleware.sanitizeCampground, CampgroundController.campgroundUpdate);

//Delete route
router.delete("/:id", Middleware.checkCampgroundOwnership, CampgroundController.campgroundDestroy);

module.exports = router;
